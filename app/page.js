import { Clock } from "@/components/Clock.jsx";

export default function Home() {
  return (
    <main>
      <Clock/>
    </main>
  )
}
