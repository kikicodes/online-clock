import './globals.css'
import { Lato } from 'next/font/google'

const lato = Lato({
  weight: ['700'],
  subsets: ['latin'],
  display: 'swap',
})

export const metadata = {
  title: 'Online clock - Erika',
  description: 'This is a React.js project building an online clock',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={lato.className}>{children}</body>
    </html>
  )
}
