"use client"

import { useState, useEffect } from "react";
import { BiSolidEyedropper } from "react-icons/bi";
import { Colors } from "./Colors";

export function Clock() {
    const [colors, setColors] = useState({
        textColor: "text-[#8062D6]",
        bgColor: "bg-[#8062D6]",
    })

    const [openColors, setOpenColors] = useState(false)

    const [dataTime, setDataTime] = useState(null)

    useEffect(() => {
        const interval = setInterval(() => {
            const date = new Date()
            setDataTime({
                hours: date.getHours(),
                minutes: date.getMinutes(),
                seconds: date.getSeconds(),
            })
        }, 1000)

        return () => clearInterval(interval)
    }, [])

    return (
        <article className="h-screen flex justify-center items-center">

            <section className="flex flex-col justify-center items-end">
                <section className="flex items-baseline justify-center gap-5">
                    <h1 className=" text-[75px] md:text-[100px] lg:text-[200px] text-white">{dataTime?.hours < 10 ? `0${dataTime?.hours}` : dataTime?.hours}:{dataTime?.minutes < 10 ? `0${dataTime?.seconds}` : dataTime?.minutes}:{dataTime?.seconds < 10 ? `0${dataTime?.seconds}` : dataTime?.seconds}</h1>

                    <h1 className={`text-[75px] md:text-[100px] lg:text-[200px] ${colors.textColor} absolute -translate-x-[34px] md:-translate-x-[42px] lg:-translate-x-[46px]`}>{dataTime?.hours < 10 ? `0${dataTime?.hours}` : dataTime?.hours}:{dataTime?.minutes < 10 ? `0${dataTime?.seconds}` : dataTime?.minutes}:{dataTime?.seconds < 10 ? `0${dataTime?.seconds}` : dataTime?.seconds}</h1>

                    <div>
                        <button className={`w-[35px] h-[35px] md:w-[50px] md:h-[50px] ${colors.bgColor} rounded-full p-2`} onClick={() => setOpenColors((prev) => !prev)}>
                            <BiSolidEyedropper className="fill-white h-full w-full" />
                        </button>
                    </div>
                </section>

                <section className="absolute translate-y-[98px] md:translate-y-[112px] lg:translate-y-[150px]">
                    <Colors setColors={setColors} openColors={openColors} />
                </section>
            </section>
        </article>
    )
}