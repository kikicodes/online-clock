export function Colors({setColors, openColors}) {
    return(
        <section className={` ${openColors ? "flex" : "hidden"} bg-white rounded-xl p-3 grid grid-cols-3 grid-rows-2 gap-3 w-[150px]`}>
            <button className="w-8 h-8 border-[4px] border-[#8062D6] bg-[#8062D6] rounded-full focus:bg-white" onClick={() => setColors(({textColor: "text-[#8062D6]", bgColor: "bg-[#8062D6]"}))}></button>

            <button className="w-8 h-8 bg-[#548CFF] rounded-full border-[4px] border-[#548CFF] focus:bg-white" onClick={() => setColors({textColor: "text-[#548CFF]", bgColor: "bg-[#548CFF]"})}></button>

            <button className="w-8 h-8 bg-[#FFBF00] rounded-full border-[4px] border-[#FFBF00] focus:bg-white" onClick={() => setColors({textColor: "text-[#FFBF00]", bgColor: "bg-[#FFBF00]"})}></button>

            <button className="w-8 h-8 bg-[#00AD7C] rounded-full border-[4px] border-[#00AD7C] focus:bg-white" onClick={() => setColors({textColor: "text-[#00AD7C]", bgColor: "bg-[#00AD7C]"})}></button>

            <button className="w-8 h-8 bg-[#EB455F] rounded-full border-[4px] border-[#EB455F] focus:bg-white" onClick={() => setColors({textColor: "text-[#EB455F]", bgColor: "bg-[#EB455F]"})}></button>

            <button className="w-8 h-8 bg-[#E966A0] rounded-full border-[4px] border-[#E966A0] focus:bg-white" onClick={() => setColors({textColor: "text-[#E966A0]", bgColor: "bg-[#E966A0]"})}></button>
        </section>
    )
}